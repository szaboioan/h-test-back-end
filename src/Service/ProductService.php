<?php

namespace App\Service;

use App\Dto\ProductDto;
use App\Entity\Product;
use App\Exception\InvalidProductData;
use App\Repository\ProductRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Respect\Validation\Validator as v;

class ProductService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @return ProductDto[]
     */
    public function getAll(): array
    {
        $products = $this->productRepository->findAll();

        return array_map(function (Product $product) {
            return ProductDto::createFromEntity($product);
        }, $products);
    }

    public function getById(int $id): ?ProductDto
    {
        $product = $this->productRepository->findOneBy(['id' => $id]);
        if ($product) {

            return ProductDto::createFromEntity($product);
        }

        return null;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws InvalidProductData
     */
    public function create(?array $productData): ProductDto
    {
        $this->validateProductData($productData);
        $productDtoToCreate = ProductDto::createFromArray($productData);
        $productEntity = $this->productRepository->create(ProductDto::toEntity($productDtoToCreate));

        return ProductDto::createFromEntity($productEntity);
    }

    /**
     * @throws Exception
     */
    public function truncate(): void
    {
        $this->productRepository->truncate();
    }

    public function isDbConnected(): bool
    {
        return $this->productRepository->isDbConnected();
    }

    /**
     * @throws InvalidProductData
     */
    private function validateProductData(?array $productData): void
    {
        $errors = [];

        if (!v::arrayType()->validate($productData) || empty($productData)) {
            $errors[] = 'Invalid product data provided';
            throw new InvalidProductData(json_encode($errors));
        }

        if (isset($productData['id']) && !v::nullable(v::intVal())->validate($productData['id'])) {
            $errors['id'] = 'Product id is invalid';
        }

        if (!isset($productData['label']) || !v::stringType()->validate($productData['label']) || !v::notEmpty()->validate($productData['label']) || !v::length(3, 255, true)->validate($productData['label'])) {
            $errors['label'] = 'Product label is invalid';
        }

        if (!isset($productData['description']) || !v::stringType()->validate($productData['description']) || !v::notEmpty()->validate($productData['description'])) {
            $errors['description'] = 'Product description is invalid';
        }

        if (!isset($productData['ean']) || !v::stringType()->validate($productData['ean']) || !v::length(13, 13, true)->validate($productData['ean'])) {
            $errors['ean'] = 'Product ean is invalid';
        }

        if (!isset($productData['price']) || !v::intVal()->positive()->validate($productData['price'])) {
            $errors['price'] = 'Product price is invalid';
        }

        if (count($errors)) {
            throw new InvalidProductData(json_encode($errors));
        }
    }
}