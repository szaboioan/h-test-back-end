<?php

namespace App\Controller;

use App\Exception\InvalidProductData;
use App\Service\ProductService;
use Doctrine\DBAL\DBALException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products", methods={"GET"})
     */
    public function getAll(ProductService $productService): Response
    {
        try {
            $products = $productService->getAll();

            return new JsonResponse($products);
        } catch (Exception $e) {
            return new JsonResponse('Something went wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/products/{id}", methods={"GET"})
     */
    public function getById(int $id, ProductService $productService): Response
    {
        try {
            $product = $productService->getById($id);
            if (!$product) {
                return new JsonResponse('Product not found', Response::HTTP_NOT_FOUND);
            }

            return new JsonResponse($product);
        } catch (Exception $e) {
            return new JsonResponse('Something went wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/products", methods={"POST"})
     */
    public function create(Request $request, ProductService $productService): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            $createdProductDto = $productService->create($data);

            return new JsonResponse($createdProductDto, Response::HTTP_CREATED);
        } catch (DBALException $e) {
            return new JsonResponse('Something went wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (InvalidProductData $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            return new JsonResponse($e->getTrace(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/products/generate", methods={"POST"})
     */
    public function generateProducts(Request $request, ProductService $productService): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            $productService->truncate();
            $createdProducts = array_map(function (array $productData) use ($productService) {
                return $productService->create($productData);
            }, $data);

            return new JsonResponse($createdProducts, Response::HTTP_CREATED);
        } catch (InvalidProductData $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/db-connection-state", methods={"GET"})
     */
    public function isDbConnected(ProductService $productService): Response
    {
        if ($productService->isDbConnected()) {
            return new JsonResponse('Db present', Response::HTTP_OK);
        }

        return new JsonResponse('No connection is present', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}