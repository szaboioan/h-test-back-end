<?php

namespace App\Dto;

use App\Entity\Product;
use JsonSerializable;

class ProductDto implements JsonSerializable
{
    private ?int $id;
    private string $label;
    private string $description;
    private string $ean;
    private int $price;

    public function __construct(?int $id, string $label, string $description, string $ean, int $price)
    {
        $this->id = $id;
        $this->label = $label;
        $this->description = $description;
        $this->ean = $ean;
        $this->price = $price;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getEan(): string
    {
        return $this->ean;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public static function createFromArray(array $productData): self
    {
        return new self(
            $productData['id'] ?? null,
            $productData['label'] ?? null,
            $productData['description'] ?? null,
            $productData['ean'] ?? null,
            $productData['price'] ?? null,
        );
    }

    public static function createFromEntity(Product $product): self
    {
        return new self($product->getId(), $product->getLabel(), $product->getDescription(), $product->getEan(), $product->getPrice());
    }

    public static function toEntity(ProductDto $productDto): Product
    {
        return (new Product())
            ->setId($productDto->getId())
            ->setLabel($productDto->getLabel())
            ->setDescription($productDto->getDescription())
            ->setEan($productDto->getEan())
            ->setPrice($productDto->getPrice());
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "label" => $this->label,
            "description" => $this->description,
            "ean" => $this->ean,
            "price" => $this->price,
        ];
    }
}