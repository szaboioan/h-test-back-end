Run the project:
- from back-end api root directory run docker-compose up -d
- after containers are up, connect to php74 container and run the following commands in order to
  create the database, and the product table:
  
        docker container exec -it php74 /bin/bash
        composer install
        php bin/console doctrine:database:create
        php bin/console doctrine:migrations:migrate
- at this point your containers should like something like this: