<?php

namespace App\Tests\Service;

use App\Dto\ProductDto;
use App\Entity\Product;
use App\Exception\InvalidProductData;
use App\Repository\ProductRepository;
use App\Service\ProductService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\TestCase;

class ProductServiceClassTest extends TestCase
{
    /**
     * @test
     */
    public function getAll(): void
    {
        $productEntity = (new Product())->setId(1)->setLabel('Phone')->setDescription('Mobile Phone')->setEan('0047532896442')->setPrice(1000);
        $productRepositoryMock = $this->createMock(ProductRepository::class);
        $productRepositoryMock->method('findAll')->willReturn([$productEntity]);
        $productService = new ProductService($productRepositoryMock);

        $this->assertEquals([new ProductDto(1, 'Phone', 'Mobile Phone', '0047532896442', 1000)], $productService->getAll());
    }

    /**
     * @test
     */
    public function getById(): void
    {
        $productEntity = (new Product())->setId(1)->setLabel('Phone')->setDescription('Mobile Phone')->setEan('0047532896442')->setPrice(1000);
        $productRepositoryMock = $this->createMock(ProductRepository::class);
        $productRepositoryMock->method('findOneBy')->willReturn($productEntity);
        $productService = new ProductService($productRepositoryMock);

        $this->assertEquals(new ProductDto(1, 'Phone', 'Mobile Phone', '0047532896442', 1000), $productService->getById(1));
    }

    /**
     * @test
     */
    public function getByIdNullResult(): void
    {
        $productRepositoryMock = $this->createMock(ProductRepository::class);
        $productRepositoryMock->method('findOneBy')->willReturn(null);
        $productService = new ProductService($productRepositoryMock);

        $this->assertEquals(null, $productService->getById(1));
    }

    /**
     * @test
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws InvalidProductData
     */
    public function create(): void
    {
        $productDataToCreate = [
            'label' => 'product label',
            'description' => 'product description',
            'ean' => '0047532896442',
            'price' => 100,
        ];
        $productRepositoryMock = $this->createMock(ProductRepository::class);
        $productRepositoryMock
            ->expects($this->once())
            ->method('create')
            ->willReturn((new Product())->setId(1)->setLabel('product label')->setDescription('product description')->setEan('0047532896442')->setPrice(100));
        $productService = new ProductService($productRepositoryMock);
        $createdProduct = $productService->create($productDataToCreate);

        $this->assertInstanceOf(ProductDto::class, $createdProduct);
        $this->assertEquals(1, $createdProduct->getId());
    }

    public function provideInvalidProductData(): array
    {
        return [
            [[]],
            [['label' => 'product label',]],
            [['label' => 'product label', 'description' => 'product description',]],
            [['label' => 'product label', 'description' => 'product description', 'ean' => '0047532896442',]],
            [['label' => 'product label', 'description' => 'product description', 'ean' => '0047532896442', 'price' => 'abc']],
            [['label' => '', 'description' => 'product description', 'ean' => '0047532896442', 'price' => 100]],
            [['label' => 'll', 'description' => 'product description', 'ean' => '0047532896442', 'price' => 100]],
            [['label' => null, 'description' => 'product description', 'ean' => '0047532896442', 'price' => 100]],
            [['label' => 'product label', 'description' => null, 'ean' => '0047532896442', 'price' => 100]],
            [['label' => 'product label', 'description' => 'product description', 'ean' => '004753289644', 'price' => 100]],
            [['label' => 'product label', 'description' => 'product description', 'ean' => null, 'price' => 100]],
        ];
    }

    /**
     * @test
     * @dataProvider provideInvalidProductData
     * @throws InvalidProductData
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createInvalidData($productDataToCreate): void
    {
        $this->expectException(InvalidProductData::class);
        $productRepositoryMock = $this->createMock(ProductRepository::class);
        $productService = new ProductService($productRepositoryMock);
        $productService->create($productDataToCreate);
    }
}